from datetime import datetime
import cherrypy.lib.static
import os
import subprocess
import tempfile

# TODO:
# Add in checks for the repository location such as file does not exist

class PyGit(object):

    # Repository location
    reploc = '/home/fletcher/git/'

    def index(self):
        html = self.makeHeader(None, None)
        html += '<table border="1">'
        html += '<tr><td width="200px"><b>Repo Name</b></td><td width="450px"><b>Description</b></td><td width="150px"><b>Last Change</b></td><td width="300px"><b>Links</b></td></tr>'
        for rep in os.listdir(self.reploc):
            if not rep.find('.git') > -1:
                cherrypy.log(rep)
                continue
            html += '<tr><td><a href="/pygit/' + rep + '/">' + rep + '</a></td>'
            html += '<td>' + self.readDesc(rep) + '</td>'
            html += '<td>' + self.lastChange(rep) + '</td>'
            html += '</tr>'
        html += ''
        html += '</table>'
        html += '</body></html>'
        return html
    index.exposed = True

    def default(self, name, commit=None, action=None, torb=None):
        # Ideally this will redirect the page. Not sure how to do this.
        if not name.find('.git') > -1:
            return self.default(name + '.git')
        if commit == 'status':
            return self.status(name)
        if not commit == None:
            if action == None:
                return self.commit(name, commit, 'summary', '')
            else:
                if torb == None:
                    return self.commit(name, commit, action, commit)
                else:
                    return self.commit(name, commit, action, torb)
        html = self.makeHeader(name, None)
        html += '<a href="status">Repository Status</a>'
        html += '<h2>Log</h2>'
        html += '<table border="1">'
        html += '<tr><td width="100px"><b>Date</b></td>'
        html += '<td width="200px"><b>Committer</b></td>'
        html += '<td width="500px"><b>Commit Message</b></td>'
        html += '<td width="400px"><b>Links</b></td></tr>'
        cmd = ['git', 'log', '--format=format:"%ci|%H|%cn|%s"', '-15', 'HEAD']
        cherrypy.log(' '.join(cmd))
        process = subprocess.Popen(cmd, stdout=subprocess.PIPE, stderr=subprocess.PIPE, cwd=self.reploc + name)
        (out, err) = process.communicate()
        lines = out.split('\n')
        for line in lines:
            line = line[1:len(line)-1]
            first = line.find('|')
            second = line.find('|', first+1)
            third = line.find('|', second+1)
            html += '<tr><td>' + line[:10] + '</td>'
            html += '<td>' + line[second+1:third] + '</td>'
            commit = line[first+1:second]
            # If its too long cut it off
            msg = line[third+1:]
            if len(msg) > 55:
                msg = msg[:52] + '...'
            html += '<td><a href="' + commit + '/">' + msg + '</a></td>'
            html += '<td><a href="'+commit+'/summary">Summary</a>&nbsp;'
            html += '<a href="'+commit+'/tarsnapshot">Tar Snapshot</a>&nbsp;'
            html += '<a href="'+commit+'/zipsnapshot">Zip Snapshot</a>&nbsp;'
            html += '<a href="'+commit+'/diff">Diff</a>&nbsp;'
            html += '<a href="'+commit+'/tree/'+commit+'">Tree</a></td></tr>'
        html += '</table></body></html>'
        return html
    default.exposed = True

    def commit(self, name, commit, action, torb):
        html = self.makeHeader(name, commit)
        html += '<table>'
        cmd = ['git', 'log', '--format=format:"%an%n%ct%n%B"', '-1', commit]
        cherrypy.log(' '.join(cmd))
        process = subprocess.Popen(cmd, stdout=subprocess.PIPE, stderr=subprocess.PIPE, cwd=self.reploc + name)
        (out, err) = process.communicate()
        out = out[1:len(out)-1]
        lines = out.split('\n')
        html += '<tr><td>Author:</td><td>' + lines[0] + '</td></tr>'
        dt = datetime.fromtimestamp(float(lines[1]))
        t = dt.strftime("%B %d, %Y %I:%M%p")
        html += '<tr><td width="100px">Date:</td><td>' + t + '</td></tr>'
        html += '<tr><td>Message:</td><td>'
        for i in range(2, len(lines)-1):
            html += lines[i] + '<br>'
        html += '</td></tr></table>'
        link = ''
        if action == 'tree':
            link = '../'
        html += '<a href="' + link + 'summary">Summary</a>&nbsp;&nbsp;'
        html += '<a href="' + link + 'tarsnapshot">Tar Snapshot</a>&nbsp;&nbsp;'
        html += '<a href="' + link + 'zipsnapshot">Zip Snapshot</a>&nbsp;&nbsp;'
        html += '<a href="' + link + 'diff">Diff</a>&nbsp;&nbsp;'
        html += '<a href="' + link + 'tree/'+commit+'">Tree</a>&nbsp;&nbsp;'
        html += '<br><br>'
        if action == 'summary':
            html += 'This is the summary page. I don\'t know what to do with this page yet.'
        if action == 'diff':
            cmd = ['git', 'log', '--format=format:""', '--no-color', '-1', '-p', commit]
            cherrypy.log(' '.join(cmd))
            process = subprocess.Popen(cmd, stdout=subprocess.PIPE, stderr=subprocess.PIPE, cwd=self.reploc + name)
            (out, err) = process.communicate()
            out = out[3:len(out)-1]
            lines = out.split('\n')
            for line in lines:
                html += '<xmp style="margin: 0px; '
                if line[:1] == '+':
                    if line[:3] == '+++':
                        html += 'font-weight: bold;'
                    else:
                        html += 'color: green;'
                elif line[:1] == '-':
                    if line[:3] == '---':
                        html += 'font-weight: bold;'
                    else:
                        html += 'color: red;'
                elif line[:10] == 'diff --git':
                    html += 'font-weight: bold;'
                elif line[:6] == 'index ':
                    html += 'font-weight: bold;'
                elif line[:3] == '@@ ':
                    html += 'color: #CC0099;'
                html += '">' + line + '</xmp>'
        if action == 'tarsnapshot' or action == 'zipsnapshot':
            f = tempfile.NamedTemporaryFile()
            cmd = ['git', 'archive']
            if action == 'tarsnapshot':
                cmd += ['--format=tar', commit]
            else:
                cmd += ['--format=zip', commit]
            cherrypy.log(' '.join(cmd))
            process = subprocess.Popen(cmd, stdout=f, cwd=self.reploc + name)
            process.wait()
            retname = name + '-' + commit
            if action == 'tarsnapshot':
                retname += '.tar'
            else:
                retname += '.zip'
            return cherrypy.lib.static.serve_file(f.name, "application/x-download", "attachment", name=retname)
        if action == 'tree':
            curr = torb
            if torb.rfind('-') > -1:
                curr = torb[torb.rfind('-')+1:]
            cherrypy.log(curr)
            cmd = ['git', 'cat-file', '-p', curr]
            cherrypy.log(' '.join(cmd))
            process = subprocess.Popen(cmd, stdout=subprocess.PIPE, stderr=subprocess.PIPE, cwd=self.reploc + name)
            (out, err) = process.communicate()
            lines = out.split('\n')
            lines.pop()
            first = lines[0][7:11]
            if commit == torb:
                new = lines[0][5:]
                cherrypy.log('YESSSSS' + new)
                return self.commit(name, commit, action, new)
            if first == 'blob' or first == 'tree':
                if not commit == torb:
                    parent = torb[:torb.rfind('-')]
                    html += '<a href="' + parent + '">..</a><br>'
                for line in lines:
                    line = line[7:]
                    if line[:4] == 'blob':
                        name = line[46:]
                        blob = line[5:46]
                        html += '<a href="' + torb + '-' + blob + '">' + name + '</a><br>'
                    if line[:4] == 'tree':
                        name = line[46:]
                        tree = line[5:46]
                        cherrypy.log(torb)
                        html += '<a href="' + torb + '-' + tree + '">' + name + '</a><br>'
            else:
                parent = torb[:torb.rfind('-')]
                html += '<a href="' + parent + '">..</a><br>'
                for line in lines:
                    html += '<xmp style="margin: 0px; background: gray;">' + line + '</xmp>'
        html += '</body></html>'
        return html

    def status(self, name):
        html = self.makeHeader(name, 'status')
        cmd = ['git', 'status', 'HEAD']
        #cmd = ['git', 'status']
        cherrypy.log(' '.join(cmd))
        process = subprocess.Popen(cmd, stdout=subprocess.PIPE, stderr=subprocess.PIPE, cwd=self.reploc + name)
        (out, err) = process.communicate()
        cherrypy.log(out)
        return html

    def makeHeader(self, name, commit):
        html = '<html><head><title>PyGit</title></head><body>'
        html += '<a href="/pygit/"><h2>Repositories</a>'
        if not name == None:
            html += '&nbsp;&nbsp;/&nbsp;&nbsp;'
            if not commit == None:
                html += '<a href="/pygit/' + name + '/">' + name + '</a>'
                html += '&nbsp;&nbsp;/&nbsp;&nbsp;' + commit
            else:
                html += name + '</h2>'
        html += '</h2>'
        return html

    def readDesc(self, name):
        # If length is to long take off characters and add ...
        f = open(self.reploc + name + '/description', 'r')
        line = f.readline()
        f.close()
        return line

    def lastChange(self, name):
        cmd = ['git', 'log', '--format=format:"%cr"', '-1', 'HEAD']
        cherrypy.log(' '.join(cmd))
        process = subprocess.Popen(cmd, stdout=subprocess.PIPE, stderr=subprocess.PIPE, cwd=self.reploc + name)
        (out, err) = process.communicate()
        out = out[1:len(out)-1]
        return out

cherrypy.config.update({'server.socket_host': '0.0.0.0',
                        'server.socket_port': 8080,
                        })

cherrypy.tree.mount(PyGit(), '/pygit')
#cherrypy.quickstart(PyGit(), '/pygit')
